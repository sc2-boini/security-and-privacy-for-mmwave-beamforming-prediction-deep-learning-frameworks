
# Security and Privacy for mmWave Beamforming Prediction Deep Learning Frameworks

## Project Overview

This repository contains the code and data for implementing security and privacy measures for mmWave beamforming prediction using deep learning frameworks. The project specifically addresses adversarial attacks and introduces mitigation strategies such as adversarial training and defensive distillation, as well as differential privacy.

## Features

- **Adversarial Attacks**: Implements FGSM and PGD attacks on deep learning models used for mmWave beamforming.
- **Mitigation Strategies**: Adversarial training and defensive distillation are employed to defend against attacks.
- **Differential Privacy**: Ensures the privacy of user data while maintaining model accuracy.

## Installation

Clone this repository:

```bash
git clone https://gitlab.com/sc2-boini/security-and-privacy-for-mmwave-beamforming-prediction-deep-learning-frameworks.git
cd security-and-privacy-for-mmwave-beamforming-prediction-deep-learning-frameworks
```

Install the required dependencies:

```bash
pip install -r requirements.txt
```

## Usage

### Data Generation

Generate the necessary datasets using MATLAB scripts available in the `DeepMIMO dataset generation` folder:

```matlab
run generate_dl_data.m
```

### Running the Model

Execute the Python scripts to simulate attacks and apply mitigation:

```bash
python run_attacks.py
python apply_defense.py
```

### Evaluate the Model

Run the evaluation scripts to analyze model performance:

```bash
python evaluate_model.py --model_path models/adversarial_model.h5
```

## Project Structure

```plaintext
├── Datasets/                 # Contains datasets generated for the project
├── DeepMIMO dataset generation/ # MATLAB scripts for dataset generation
├── Matlab Graphs Generation/ # Scripts for generating result plots
├── README.md                 # Project overview and instructions
└── dissertation_code.ipynb   # Jupyter notebook for model implementation
```

## Results

Results including RMSE plots and learning curves are stored in the `Matlab Graphs Generation` folder.

### Example Result Plot

![Example Plot](images/example_plot.png)

## Contributors

- [Sathish Boini](https://gitlab.com/sc2-boini)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
